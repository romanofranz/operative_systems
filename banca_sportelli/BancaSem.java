package banca_sportelli;

import java.util.Arrays;
import java.util.concurrent.Semaphore;

public class BancaSem extends Banca {
	
	private Semaphore mutex;
	private Semaphore[] bancomat;
	
	public BancaSem(int c){
		super(c);
		mutex=new Semaphore(1);
		bancomat=new Semaphore[c];
		for(int i=0;i<c;i++){
			bancomat[i]=new Semaphore(1,true);
		}
		
	}

	@Override
	public void ricarica(int sportello) throws InterruptedException {
		bancomat[sportello].acquire();
		Thread.sleep(50); 	//tempo impiegato per le operazioni
		mutex.acquire();
		sportelli[sportello]=10000;
		System.out.println("L'agente ricarica lo sportello "+ sportello);
		System.out.println(" valori attuali= "+Arrays.toString(sportelli) );
		mutex.release();
		bancomat[sportello].release();
	}

	@Override
	public boolean preleva(int s, int v) throws InterruptedException {
		bancomat[s].acquire();
		mutex.acquire();
		if(sportelli[s]<v){
			System.out.println("Il cliente "+Thread.currentThread().getId()+ " non puo' prelevare "+v+" $ dallo sportello "+s);
			System.out.println(" valori attuali= "+Arrays.toString(sportelli) );
			mutex.release();
			return false;
		}else{
			assert (sportelli[s]>=v) : "operazione invalida";
			Thread.sleep(5); 	//tempo impiegato per le operazioni
			sportelli[s]=sportelli[s]-v;
			System.out.println("Il cliente "+Thread.currentThread().getId()+ " preleva "+v+" $ dallo sportello "+s);
			System.out.println(" valori attuali= "+Arrays.toString(sportelli) );
			mutex.release();
			return true;
		}
	}

	public void lasciaSportello(int s) throws InterruptedException {
		bancomat[s].release();
	}

}
