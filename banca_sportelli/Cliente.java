package banca_sportelli;

import java.util.Random;

public class Cliente extends Thread {
	private Random r;
	private Banca banca;
	
	public Cliente(Banca b){
		banca=b;
		r=new Random();
	}
	
	public void run(){
		while(true){
			try{
				int s= sportelloCasuale();
				Thread.sleep(10);
				int v=valoreCasuale();
				boolean prelevato=banca.preleva(s,v);
				banca.lasciaSportello(s);
				if(prelevato)
					Thread.sleep(v);
			}catch (InterruptedException e){
				e.printStackTrace();
			}
		}
	}

	private int valoreCasuale() {
		return r.nextInt(250-50+1)+50;
	}

	private int sportelloCasuale() {
		return r.nextInt(30);
	}

}
