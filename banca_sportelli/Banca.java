package banca_sportelli;

public abstract class Banca {
	
	protected int[] sportelli;
	
	public Banca(int c){
		sportelli=new int[c];
		for(int i=0;i<sportelli.length;i++){
			sportelli[i]=10000;
		}
	}

	public int getMin() {
		int m=sportelli[0];
		int c=0;
		for(int i=1;i<sportelli.length;i++){
			if(sportelli[i]<m){
				m=sportelli[i];
				c=i;
			}
		}return c;
	}

	public abstract void ricarica(int sportello) throws InterruptedException;

	public abstract boolean preleva(int s, int v) throws InterruptedException;

	public abstract void lasciaSportello(int s)throws InterruptedException ;
	
	public static void main(String...dannatoMultiThreading){
		Banca banca=new BancaLC(30);
		Agente smith=new Agente(banca);
		Cliente [] massa=new Cliente[100];
		smith.start();
		for(int i =0;i<100;i++){
			massa[i]=new Cliente(banca);
			massa[i].start();
		}
	}
	

}
