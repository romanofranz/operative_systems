package banca_sportelli;

public class Agente extends Thread {
	
	private Banca banca;
	
	public Agente(Banca b){
		banca=b;
	}
	
	public void run(){
		while(true){
			try {
				int sportello=banca.getMin();
				Thread.sleep(10);
				banca.ricarica(sportello);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
	}

}
