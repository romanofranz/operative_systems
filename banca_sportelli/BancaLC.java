package banca_sportelli;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BancaLC extends Banca {
	
	private Lock mutex;
	private Lock []bancomat;
	private Condition [] bancomatCond;
	@SuppressWarnings("rawtypes")
	private LinkedList [] code;
	
	public BancaLC(int c){
		super(c);
		mutex=new ReentrantLock();
		bancomat= new Lock[c];
		bancomatCond= new Condition[c];
		code=new LinkedList[c];
		for(int i =0;i<c;i++){
			bancomat[i]=new ReentrantLock(true);
			bancomatCond[i]=bancomat[i].newCondition();
			code[i]=new LinkedList<Thread>();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void ricarica(int sportello) throws InterruptedException {
		try{
			bancomat[sportello].lock();
			code[sportello].addLast(Thread.currentThread());
			while(!mioTurno(sportello))
				bancomatCond[sportello].await();
			code[sportello].removeFirst();
			Thread.sleep(50); 	//tempo impiegato per le operazioni
			mutex.lock();
			sportelli[sportello]=10000;
			System.out.println("L'agente ricarica lo sportello "+ sportello);
			System.out.println(" valori attuali= "+Arrays.toString(sportelli) );
		}finally{
			bancomat[sportello].unlock();
			mutex.unlock();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean preleva(int s, int v) throws InterruptedException {
		try{
			bancomat[s].lock();
			code[s].addLast(Thread.currentThread());
			while(!mioTurno(s))
				bancomatCond[s].await();
			code[s].removeFirst();
			mutex.lock();
			if(sportelli[s]<v){
				System.out.println("Il cliente "+Thread.currentThread().getId()+ " non puo' prelevare "+v+" $ dallo sportello "+s);
				System.out.println(" valori attuali= "+Arrays.toString(sportelli) );
				return false;
			}else{
				assert (sportelli[s]>=v) : "operazione invalida";
				Thread.sleep(5); 	//tempo impiegato per le operazioni
				sportelli[s]=sportelli[s]-v;
				System.out.println("Il cliente "+Thread.currentThread().getId()+ " preleva "+v+" $ dallo sportello "+s);
				System.out.println(" valori attuali= "+Arrays.toString(sportelli) );
				return true;
			}
				}finally{
					bancomat[s].unlock();
					mutex.unlock();
		}
	}

	private boolean mioTurno(int s) {
		return code[s].getFirst()==Thread.currentThread();
	}

	@Override
	public void lasciaSportello(int s) throws InterruptedException {
		try{
			bancomat[s].lock();
			bancomatCond[s].signalAll();
		}finally{
			bancomat[s].unlock();
		}
		

	}

}
