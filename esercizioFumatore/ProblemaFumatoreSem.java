package esercizioFumatore;

import java.util.Random;
import java.util.concurrent.Semaphore;

public class ProblemaFumatoreSem extends ProblemaFumatore{
	private Semaphore cartine=new Semaphore(0);
	private Semaphore tabacco=new Semaphore(0);
	private Semaphore fiammiferi=new Semaphore(0);
	private Semaphore mutex=new Semaphore(1);

	private Random random=new Random();
	
	
	
	public void generaElementi() throws InterruptedException{
		mutex.acquire();
		switch(random.nextInt(3)){
		case 0: generaTabacco();
				generaCartine();
				break;
		case 1: generaTabacco();
				generaFiammiferi();
				break;
		case 2: generaCartine();
				generaFiammiferi();
				break;
		}
		mutex.release();	
	}
	
	void generaTabacco(){
		System.out.println("Thread agente mette il tabacco sul tavolo");
		isThereTabacco=true;
		tabacco.release();
	}
	void generaCartine(){
		System.out.println("Thread agente mette le cartine sul tavolo");
		isThereCartine=true;
		cartine.release();
	}
	void generaFiammiferi(){
		System.out.println("Thread agente mette i fiammiferi sul tavolo");
		isThereFiammiferi=true;
		fiammiferi.release();
	}
	
	public void ottieniFiammiferi() throws InterruptedException{
		fiammiferi.acquire();
	}
	public void ottieniTabacco() throws InterruptedException{
		tabacco.acquire();
	}
	public void ottieniCartine() throws InterruptedException{
		cartine.acquire();
	}
	public void ottieniTavolo() throws InterruptedException{
		mutex.acquire();
	}
	public void lasciaTavolo(){
		mutex.release();
	}



	public static void main(String[] args) {
		try{
		ProblemaFumatore fum = new ProblemaFumatoreSem();
		fum.test();
		}catch(Exception e ){
			e.printStackTrace();
		}

	}

}
