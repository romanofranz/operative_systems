package esercizioFumatore;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Fumatore implements Runnable {
	private static final int MIN_TEMPO_ARROTOLA= 1;
	private static final int MAX_TEMPO_ARROTOLA = 3;
	private static final int MIN_TEMPO_FUMA =4 ;
	private static final int MAX_TEMPO_FUMA = 6;

	private ProblemaFumatore tavolo;
	private Random random = new Random();
	String oggettoIniziale;

	public Fumatore(ProblemaFumatore tav , String iniz) {
		tavolo=tav;
		oggettoIniziale=new String(iniz);
	}
	public void run() {
		while(true){
			try {
				if (oggettoIniziale.equals(tavolo.t)){
					while(tavolo.isThereCartine && tavolo.isThereFiammiferi){
						tavolo.ottieniTavolo();
						tavolo.ottieniCartine();
						tavolo.ottieniFiammiferi();
						arrotola();
						fuma();
						tavolo.isThereCartine=false;
						tavolo.isThereFiammiferi=false;
						tavolo.chiamaAgente=true;
						tavolo.lasciaTavolo();
					}
				}else if (oggettoIniziale.equals(tavolo.c)){
					while(tavolo.isThereTabacco && tavolo.isThereFiammiferi){
						tavolo.ottieniTavolo();
						tavolo.ottieniTabacco();
						tavolo.ottieniFiammiferi();
						arrotola();
						fuma();
						tavolo.isThereTabacco=false;
						tavolo.isThereFiammiferi=false;
						tavolo.chiamaAgente=true;
						tavolo.lasciaTavolo();
					}
				}else{
					while(tavolo.isThereTabacco && tavolo.isThereCartine){
						tavolo.ottieniTavolo();
						tavolo.ottieniTabacco();
						tavolo.ottieniCartine();
						arrotola();
						fuma();
						tavolo.isThereTabacco=false;
						tavolo.isThereCartine=false;
						tavolo.chiamaAgente=true;
						tavolo.lasciaTavolo();
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}//while true
	}
	private void arrotola() throws InterruptedException {
		attendi(MIN_TEMPO_ARROTOLA, MAX_TEMPO_ARROTOLA);
		System.out.println("Thread "+ Thread.currentThread().getId() +" con "+oggettoIniziale  + " si sta arrotolando una sigaretta...");
	}
	private void fuma() throws InterruptedException {
		attendi(MIN_TEMPO_FUMA, MAX_TEMPO_FUMA);
		System.out.println("Thread "+Thread.currentThread().getId() +" con " +oggettoIniziale+ " sta fumando...");
	}
	private void attendi(int min, int max) throws InterruptedException {
		TimeUnit.SECONDS.sleep(random.nextInt(max - min + 1) + min);
	}

}
