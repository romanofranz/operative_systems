package esercizioFumatore;

import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ProblemaFumatoreMonitor extends ProblemaFumatore {
	private Lock lock=new ReentrantLock();
	private Condition agent=lock.newCondition();
	private Condition tabacco=lock.newCondition();
	private Condition cartine=lock.newCondition();
	private Condition fiammiferi=lock.newCondition();
	private Random random=new Random();
	
	public void ottieniCartine() throws InterruptedException{
		cartine.await();
	}
	public void ottieniTabacco() throws InterruptedException{
		tabacco.await();
	}
	public void ottieniFiammiferi() throws InterruptedException{
		fiammiferi.await();
	}
	
	public void ottieniTavolo() throws InterruptedException{
		lock.lock();
	}
	public void lasciaTavolo() {
		lock.unlock();
	}
	
	public void generaElementi(){
		try {
			lock.lock();
		//	agent.await();
			switch(random.nextInt(3)){
			case 0: generaTabacco();
					generaCartine();
					break;
			case 1: generaTabacco();
					generaFiammiferi();
					break;
			case 2: generaCartine();
					generaFiammiferi();
					break;
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			lock.unlock();
		}
		
	}
	
	void generaTabacco(){
		System.out.println("Thread agente mette il tabacco sul tavolo");
		isThereTabacco=true;
		tabacco.signalAll();
	}
	void generaCartine(){
		System.out.println("Thread agente mette le cartine sul tavolo");
		isThereCartine=true;
		cartine.signalAll();

	}
	void generaFiammiferi(){
		System.out.println("Thread agente mette i fiammiferi sul tavolo");
		isThereCartine=true;
		fiammiferi.signalAll();
	}
	
	


	
	public static void main(String[] args) {
		try{
			ProblemaFumatore fum = new ProblemaFumatoreSem();
			fum.test();
			}catch(Exception e ){
				e.printStackTrace();
			}

	}

}
