package esercizioFumatore;

public class Agente implements Runnable {
	
	private ProblemaFumatore tavolo;

	public Agente(ProblemaFumatore tav){
		tavolo=tav;
	}
	public void run(){
		while(true){
			try{
				while(tavolo.chiamaAgente){
					tavolo.generaElementi();
					tavolo.chiamaAgente=false;
				}
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
		
	}
}
