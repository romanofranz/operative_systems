package esercizioFumatore;

public abstract class ProblemaFumatore {

	public abstract void ottieniTavolo() throws InterruptedException;
	public abstract void ottieniCartine() throws InterruptedException;
	public abstract void ottieniTabacco() throws InterruptedException;
	public abstract void ottieniFiammiferi() throws InterruptedException;
	public abstract void lasciaTavolo();
	public abstract void generaElementi() throws InterruptedException;
	
	public boolean chiamaAgente=true;
	public boolean isThereTabacco=false;
	public boolean isThereCartine=false;
	public boolean isThereFiammiferi=false;
	
	public String t="tabacco";
	public String c="cartine";
	public String f="fiammiferi";

	
	protected void test(){
		Fumatore a = new Fumatore(this,c);
		Fumatore b= new Fumatore(this,t);
		Fumatore c = new Fumatore(this,f);
		Agente x =new Agente(this);
		
		System.out.println("Test della classe " + getClass().getSimpleName());
		Thread xt=new Thread(x);
		Thread bt=new Thread(b);
		Thread ct=new Thread(c);
		Thread at=new Thread(a);
		xt.start();
		at.start();	bt.start();	ct.start();
	}


}
