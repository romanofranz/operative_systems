package appello21luglio2011;

import java.util.concurrent.Semaphore;

public class IngressoSem extends Ingresso {
	
	private Semaphore enter;
	
	public IngressoSem(int c){
		super(c);
		enter=new Semaphore(c);
	}
	
	public void ottieniIngresso() throws InterruptedException{
		enter.acquire();
	}
	public void lasciaIngresso(){
		enter.release();
	}
	
	public static void test(int x , int y, int z){
		Formica [] f= new Formica[x];
		Ingresso i = new IngressoSem(z);
		Formicaio fo = new FormicaioSem(y);
		for(Formica a : f){
			a=new Formica(i,fo);
			a.start();
		}
	}


	public static void main(String[] args) {
		test(30,20,10);
	}

}
