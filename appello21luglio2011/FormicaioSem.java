package appello21luglio2011;

import java.util.concurrent.Semaphore;

public class FormicaioSem extends Formicaio {
	private Semaphore larve;
	
	public FormicaioSem(int n){
		super(n);
		larve=new Semaphore(n,true);
	}
	
	public void nutriLarva() throws InterruptedException{
		larve.acquire();
		System.out.println("Formica "+Thread.currentThread().getId()+ " sta nutrendo una larva");
		Thread.sleep(500);
		larve.release();
	}
	

}
