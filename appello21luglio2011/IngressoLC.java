package appello21luglio2011;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class IngressoLC extends Ingresso {
	public IngressoLC(int x){
		super(x);
	}
	
	private Lock lock=new ReentrantLock();
	
	private Condition possoAccedere=lock.newCondition();
	
	public void ottieniIngresso() throws InterruptedException{
		lock.lock();
		try{
			while(postiLiberi==0){
				possoAccedere.await();
			}
			postiLiberi--;
		}finally{
			lock.unlock();
		}
	}
	
	public void lasciaIngresso(){
		lock.lock();
		try{
			postiLiberi++;
			possoAccedere.signalAll();
		}finally{
			lock.unlock();
		}
	}
	public static void test(int x , int y, int z){
		Formica [] f= new Formica[x];
		Ingresso i = new IngressoLC(z);
		Formicaio fo = new FormicaioLC(y);
		for(Formica a : f){
			a=new Formica(i,fo);
			a.start();
		}
	}
	public static void main(String...a){
		test(30,20,10);
	}

}
