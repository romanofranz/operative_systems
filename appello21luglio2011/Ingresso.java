package appello21luglio2011;

public abstract class Ingresso {
	int capienza;
	int postiLiberi;
	public Ingresso(int c){
		capienza=c;
		postiLiberi=c;
	}
	
	public abstract void ottieniIngresso() throws InterruptedException;
	public abstract void lasciaIngresso() throws InterruptedException;

}
