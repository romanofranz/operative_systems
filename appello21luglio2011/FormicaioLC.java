package appello21luglio2011;

import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class FormicaioLC extends Formicaio {
	
	public FormicaioLC(int n){
		super(n);
	}
	
	private Lock lock= new ReentrantLock(true);
	
	private Condition larvaOccupata = lock.newCondition();
	
	private LinkedList<Thread> coda= new LinkedList<Thread>();
	
	
	public void nutriLarva() throws InterruptedException{
		lock.lock();
		try{
			larvaOccupata.signalAll();
			coda.addLast(Thread.currentThread());
			while(!turnoFormica()){
				larvaOccupata.await();
			}
			coda.removeFirst();
			larveLibere--;
			System.out.println("Formica "+Thread.currentThread().getId()+ " sta nutrendo una larva");
			Thread.sleep(500);
			larveLibere++;
			larvaOccupata.signalAll();
		}finally{
			lock.unlock();
		}
	}
	
	boolean turnoFormica(){
		return larveLibere>0 && coda.getFirst() == Thread.currentThread();
	}
	
	

}
