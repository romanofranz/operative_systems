package appello21luglio2011;

import java.util.Random;

public class Formica extends Thread {
	
	private Ingresso ingresso;
	private Formicaio formicaio;
	private static int MAX=70;
	private static int MIN=30;
	private Random random;
	
	public Formica(Ingresso i,Formicaio f){
		ingresso=i;
		formicaio=f;
		random=new Random();
	}
	
	public void run(){
		while(true){
			try{
				cercaCibo();
				ingresso.ottieniIngresso();
				Thread.sleep(40);
				ingresso.lasciaIngresso();
				formicaio.nutriLarva();
				ingresso.ottieniIngresso();
				Thread.sleep(40);
				ingresso.lasciaIngresso();
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
	}
	
	void cercaCibo()throws InterruptedException{
		Thread.sleep(random.nextInt(MAX- MIN + 1) + MIN);
	}

}
