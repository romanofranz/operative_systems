package esercizio2;

public class TrovaMin extends Thread {

	private int min,col;
	private int [][]M;
	
	public TrovaMin(int[][]M,int col){
		this.col=col;
		this.M=M;
		min=M[0][col];
	}
	
	public void run(){
		for(int i=1;i<M.length;i++)
			if(M[i][col]<min)
				min=M[i][col];
	}
	
	public int getMin(){
		try {
			this.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return min;
		
	}

}
