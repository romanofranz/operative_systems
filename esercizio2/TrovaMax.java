package esercizio2;

public class TrovaMax extends Thread {
	private int max,riga;
	private int [][]M;
	
	public TrovaMax(int[][]M,int riga){
		this.riga=riga;
		this.M=M;
		max=M[riga][0];
	}
	
	public void run(){
		for(int i=1;i<M[riga].length;i++)
			if(M[riga][i]>max)
				max=M[riga][i];
	}
	
	public int getMax(){
		try {
			this.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return max;
		
	}

}
