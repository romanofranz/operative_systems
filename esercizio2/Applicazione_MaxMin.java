package esercizio2;

public class Applicazione_MaxMin {

	public static void main(String[] args) {
		int [][] M=new int[10][10];
		int n=10,m=10;
		TrovaMax [] massimi = new TrovaMax[n];
		TrovaMin [] minimi = new TrovaMin[m];
		for(int i=0;i<n;i++){
			massimi[i]= new TrovaMax(M,i);
			massimi[i].start();
		}
		for(int j=0;j<m;j++){
			minimi[j]=new TrovaMin(M,j);
			minimi[j].start();
		}
		
		int rig,col;
		
		for(int i=0;i<n;i++){
			for(int j=0;j<m;j++){
				col=massimi[i].getMax();
				rig=minimi[j].getMin();
				if (rig == i && col == j) {
					System.out.println("Il primo elemento massimo di riga e" +
						"minimo di colonna occupa la posizione (" + rig + "," + col + ").");
					return;
				}
			}
		}

	}

}
