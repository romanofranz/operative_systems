package ristorante;

import java.util.concurrent.Semaphore;

public class BoxSem extends Box {
	private Semaphore ciSonoPiatti= new Semaphore(0);
	private Semaphore ciSonoPosti;
	private Semaphore mutex =new Semaphore(1);
	
	public BoxSem(int x){
		super(x);
		ciSonoPosti=new Semaphore(x);
	}
	
	public void put(int n) throws InterruptedException{
		ciSonoPosti.acquire(n);
		mutex.acquire();
		this.size=size+n;
		if(size()>dimMax){
			System.out.println("Errore size="+size()+ " dimMax="+dimMax);
		}
		mutex.release();
		ciSonoPiatti.release(n);
	}
	
	public void get() throws InterruptedException{
		ciSonoPiatti.acquire();
		mutex.acquire();
		this.size--;
		if(size()>dimMax){
			System.out.println("Errore size="+size()+ " dimMax="+dimMax);
		}
		mutex.release();
		ciSonoPosti.release();
	}

}
