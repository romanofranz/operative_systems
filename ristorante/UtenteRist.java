package ristorante;

import java.util.concurrent.TimeUnit;

public class UtenteRist extends Thread {
	
	private int tempoWait,tipo;
	private Box box;
	
	public UtenteRist(int tempoWait ,Box box , int tipo){
		this.tempoWait=tempoWait;
		this.box=box;
		this.tipo=tipo;
	}
	
	public void run(){
		while(true){
			try{
				TimeUnit.SECONDS.sleep(tempoWait);
				if(tipo==0/*cameriere*/){
					box.put(4);
					System.out.println("Cameriere "+Thread.currentThread().getId()+ " ha messo 4 piatti nel contenitore..");
				}else if(tipo==1 /*asciugapiatti*/){
					box.get();
					System.out.println("Asciugapiatti "+Thread.currentThread().getId()+ " asciuga un piatto...");

				}
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
	}

}
