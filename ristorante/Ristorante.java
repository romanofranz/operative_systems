package ristorante;

public class Ristorante {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		BoxSem contenitore=new BoxSem(50);
		BoxSem scolapiatti=new BoxSem(30);
		UtenteRist[] camerieri = new UtenteRist[10];
		Lavapiatti[] lavaPiatti = new Lavapiatti[6];
		UtenteRist[] asciugaPiatti = new UtenteRist[3];
		for(UtenteRist x : camerieri){
			x=new UtenteRist(20,contenitore,0);
			x.start();
		}
		for(Lavapiatti y : lavaPiatti){
			y=new Lavapiatti(contenitore,scolapiatti);
			y.start();
		}
		for(UtenteRist z : asciugaPiatti){
			z=new UtenteRist(10,scolapiatti,1);
			z.start();
		}

	}
}
