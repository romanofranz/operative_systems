package ristorante;

public abstract class Box {
	int dimMax;
	int size;
	public Box(int dimMax){
		this.dimMax=dimMax;
	}
	public abstract void put(int n) throws InterruptedException;
	public abstract void get() throws InterruptedException;
	
	public int size(){
		return size;
	}
	
}
