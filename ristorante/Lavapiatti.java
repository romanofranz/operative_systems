package ristorante;

import java.util.concurrent.TimeUnit;

public class Lavapiatti extends Thread {
	private int tempoWait=15;
	private Box contenitore,scolapiatti;
	
	public Lavapiatti(Box c , Box s){
		this.contenitore=c;
		this.scolapiatti=s;
	}
	
	public void run(){
		while(true){
			try {
				contenitore.get();
				System.out.println("Lavapiatti "+Thread.currentThread().getId()+ " sta lavando un piatto..");
				TimeUnit.SECONDS.sleep(tempoWait);
				System.out.println("Lavapiatti "+Thread.currentThread().getId()+ " mette un piatto nello scolapiatti..");
				scolapiatti.put(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
