/**
 * 
 */
package esercizio7;

import java.util.concurrent.Semaphore;

/**
 * @author romanofranz
 *
 */
public class LettoriScrittoriConAging extends MemoriaCondivisa{
	Semaphore mutex = new Semaphore(1);
	Semaphore scrittura = new Semaphore(1);
	int min=Thread.MIN_PRIORITY;
	
	private int nLettori=0;
	
	public void inizioLettura() throws InterruptedException{
		mutex.acquire();
		Thread me=Thread.currentThread();
		/*if(me.getPriority()==min){
			mutex.release();*/
		if(me.getPriority()>min){
			me.setPriority(me.getPriority()-1);
		}
		if (nLettori == 0) scrittura.acquire();
		nLettori++;
		System.out.println("Thread " + Thread.currentThread().getId() + " inizia a leggere!");
		mutex.release();
			
	}
	
	public void fineLettura() throws InterruptedException {
		mutex.acquire();
		nLettori--;
		System.out.println("Thread " + Thread.currentThread().getId() + " finisce di leggere");
		if (nLettori == 0) scrittura.release();
		mutex.release();
	}
	
	public void inizioScrittura() throws InterruptedException {
		scrittura.acquire();
		Thread me=Thread.currentThread();
		me.setPriority(Thread.MAX_PRIORITY);

		System.out.println("Thread " + Thread.currentThread().getId() + " inizia a scrivere!");
		System.out.println("\tNumero lettori: " + nLettori);
	}

	public void fineScrittura() throws InterruptedException {
		System.out.println("\tNumero lettori: " + nLettori);
		System.out.println("Thread " + Thread.currentThread().getId() + " finisce di scrivere");
		scrittura.release();
	}

	
	
	public static void main(String[] args) {
		LettoriScrittoriConAging test= new LettoriScrittoriConAging();
		test.test(20,5);

	}

}
