package aule_esami;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Studente extends Thread {
	int MIN=45;
	int MAX=60;
	
	Aule aule;
	Random random;
	
	public Studente(Aule a){
		aule=a;
		random=new Random();
	}
	
	public void run(){
		while(true){
			try {
				int aula=aule.entra();
				svolgiEsame();
				aule.esci(aula);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	void svolgiEsame() throws InterruptedException{
		TimeUnit.MINUTES.sleep(random.nextInt(MAX-MIN+1)-MIN);
	}

	

}
