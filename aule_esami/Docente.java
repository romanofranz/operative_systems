package aule_esami;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Docente extends Thread {
	
	private static final int SECONDI_ATTESA = 5;

	private Random random = new Random();

	private Aule aule;

	public Docente(Aule esame) {
		this.aule = esame;
	}

	@Override
	public void run() {
		try {
			while (true) {
				aule.chiamaStudente();
				attendi();
			}
		} catch (InterruptedException e) {
		}
	}

	private void attendi() throws InterruptedException {
		TimeUnit.SECONDS.sleep(random.nextInt(SECONDI_ATTESA));
	}

}
