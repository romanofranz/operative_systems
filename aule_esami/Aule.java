package aule_esami;

public abstract class Aule {
	int numAule;
	int aulaAttuale=0;
	int [] postiMassimi= {80 , 60 , 40 };
	int [] postiDisponibili={80 , 60 , 40 };
	
	public Aule(){
		
	}
	
	
	public abstract int entra();
	public abstract void esci(int aula);
	public abstract void chiamaStudente();
	
	public void aulaAttuale(){
		
	}
}
