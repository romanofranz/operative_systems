package algoritmo_del_banchiere;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Banchiere {
	
	private int n; //numero di processi
	private int m; //tipi di risorse
	private int[] available;
	private int [][] max;
	private int [][]allocation;
	private int [][] need;
	
	public int getM(){
		return m;
	}
	
	public Banchiere(int n ,int m){
		this.n=n;
		this.m=m;
		available=new int[m];
		max=new int[n][m];
		allocation=new int[n][m];
		need=new int[n][m];
		for(int j=0;j<n;j++){
			need[j]=sottrai(max[j],allocation[j]);
		}
	}
	
	public synchronized boolean assegnaRisorse(int [] request , int id){
		if(maggiore(request,need[id])){
			System.err.println("il processo "+Thread.currentThread().getId()+" ha superato il numero massimo di richiese");
			return false;
		}
		if(maggiore(request,available))
			return false;
		simulaAssegnazione(request,id);
		if(verificaStatoSicuro()){
			System.out.println("il processo "+Thread.currentThread().getId()+" e' andato a buon fine con richieste "+Arrays.toString(request));
			stampaStato();
			return true;
		}
		annullaSimulazione(request,id);
		System.out.println("il processo "+Thread.currentThread().getId()+" NON e' andato a buon fine con richieste "+Arrays.toString(request));
		return false;
		
	}
	
	 boolean maggiore(int []a , int []b){
		 for(int i=0;i<a.length;i++){
			 if(a[i]<=b[i])
				 return false;
		 }
		 return true;
	}
	 
	void simulaAssegnazione(int [] request, int id){
		available=sottrai(available,request);
		allocation[id]=somma(allocation[id],request);
		need[id]=sottrai(need[id],request);
		
	}
	
	synchronized void annullaSimulazione(int []request ,int id){
		available=somma(available , request);
		allocation[id]=sottrai(allocation[id],request);
		need[id]=somma(need[id],request);
		stampaStato();
	}
	
	int [] somma(int[]a , int[]b){
		int [] ris =new int [a.length];
		for(int i=0;i<a.length;i++){
			ris[i]=a[i]+b[i];
		}
		return ris;
	}
	
	int [] sottrai(int[]a , int[]b){
		int [] ris =new int [a.length];
		for(int i=0;i<a.length;i++){
			ris[i]=a[i]-b[i];
		}
		return ris;
	}
	
	boolean verificaStatoSicuro(){
		int [] work = new int[m];
		for(int i=0;i<m;i++)
			work[i]=available[i];
		boolean [] finish =new boolean[n];
		
		int j=0;
		while(j<n){
			if(!finish[j] && !maggiore(need[j],work)){
				work=somma(work,allocation[j]);
				finish[j]=true;
				j++;
			}
		}
		return tuttoVero(finish);
	}
	
	boolean tuttoVero(boolean[]x){
		for(boolean b :x)
			if(!b)
				return false;
		return true;
	}
	
	synchronized void stampaStato(){
		System.out.println("available");
		System.out.println(Arrays.toString(available));
		System.out.println("max");
		for(int [] i :max)
			System.out.println(Arrays.toString(i));
		System.out.println("allocation");
		for(int [] y : allocation)
			System.out.println(Arrays.toString(y));
		System.out.println("need");
		for(int [] z :need)
			System.out.println(Arrays.toString(z));
		
	}
	
	class Processo extends Thread{
		int [] request;
		Random random;
		
		Processo(){
			request=new int[m];
			random=new Random();
			
		}
		
		public void run(){
			while(true){
				for(int i=0;i<m;i++)
					request[i]=random.nextInt(m);
				assegnaRisorse(request,(int)Thread.currentThread().getId());
				try {
					TimeUnit.SECONDS.sleep(5);
					System.out.println("");
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				annullaSimulazione(request,(int)Thread.currentThread().getId());
				
			}
		}
		
	}
	
	public static void main(String[]args){
		
	}

}
