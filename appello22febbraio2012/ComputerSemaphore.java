package appello22febbraio2012;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class ComputerSemaphore extends Computer {
	
	private Semaphore core_di_0 = new Semaphore(4,true);
	private Semaphore core_di_1 = new Semaphore(4,true);
	private Semaphore mutex=new Semaphore(1);
	//private Semaphore processore0=new Semaphore(0,true);
	//private Semaphore processore1=new Semaphore(0,true);
	
	
	public void ottieniCore(int c,int p) throws InterruptedException{
		mutex.acquire();
		if(p==0){
			if(core0liberi-c>=0){
				core_di_0.acquire(c);
				core0liberi-=c;
				ottenuti=true;
				System.out.println("Utente"+Thread.currentThread().getId()+ " sta usando "+c+" core di "+p);
				System.out.println("core liberi di 0 = "+core0liberi);
				System.out.println("core liberi di 1 = "+core1liberi);
				TimeUnit.SECONDS.sleep(1);
				mutex.release();
			}else{
				ottenuti=false;
				mutex.release();
			}
			
		}else{
			if(core1liberi-c>=0){
				core_di_1.acquire(c);
				core1liberi-=c;
				ottenuti=true;
				System.out.println("Utente"+Thread.currentThread().getId()+ " sta usando "+c+" core di "+p);
				System.out.println("core liberi di 0 = "+core0liberi);
				System.out.println("core liberi di 1 = "+core1liberi);
				TimeUnit.SECONDS.sleep(1);
				mutex.release();
			}else{
				ottenuti=false;
				mutex.release();
			}
		}
			
	}
	
	
	
	public void rilasciaCore(int c,int p) throws InterruptedException{
		mutex.acquire();
		if(p==0){
			core_di_0.release(c);
			core0liberi+=c;
			System.out.println("Utente"+Thread.currentThread().getId()+ " rilascia "+c+" core di "+p);
			System.out.println("core liberi di 0 = "+core0liberi);
			System.out.println("core liberi di 1 = "+core1liberi);
		}else{
			core_di_1.release(c);
			core1liberi+=c;
			System.out.println("Utente"+Thread.currentThread().getId()+ " rilascia "+c+" core di "+p);
			System.out.println("core liberi di 0 = "+core0liberi);
			System.out.println("core liberi di 1 = "+core1liberi);
		}
		mutex.release();
	}
	
	
	
	public static void main(String[]args){
		Computer pc=new ComputerSemaphore();
		Utente[] utenti = new Utente[20];
		for(Utente x : utenti){
			x=new Utente(pc);
			x.start();
		}
	}
	
	
}
