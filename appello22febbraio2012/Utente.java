package appello22febbraio2012;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Utente extends Thread {
	
	private Computer computer;
	private Random random;
	private static int MAX=4;
	private static int MIN=1;
	private int core,processore;
	//private boolean ottenuti=false;
	
	public Utente(Computer pc){
		computer=pc;
		random=new Random();
	}
	
	
	public void run(){
		while(true){
			core=coreCasuali();
			processore=processoreCasuale();
	loop:	for(;;){
			try{
				computer.ottieniCore(core,processore);
				if(!computer.ottenuti){
					continue loop;
				}else{
					/*
					 * System.out.println("Utente"+Thread.currentThread().getId()+ " sta usando "+core+" core di "+processore);
					System.out.println("core liberi di 0 = "+computer.core0liberi);
					System.out.println("core liberi di 1 = "+computer.core1liberi);
					TimeUnit.SECONDS.sleep(1);
					*/
					computer.rilasciaCore(core,processore);
				}
				TimeUnit.SECONDS.sleep(5);
			}catch(InterruptedException e){
				e.printStackTrace();
			}
			}//for
		}
		
	}
	
	int coreCasuali(){
		return random.nextInt(MAX- MIN + 1) + MIN;
	}
	
	int processoreCasuale(){
		return random.nextInt(2);
	}

}
