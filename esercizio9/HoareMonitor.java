package esercizio9;

import java.util.concurrent.Semaphore;

public class HoareMonitor  {
	private Semaphore mutex=new Semaphore(1);
	private Semaphore prossimo =new Semaphore(0);
	private Semaphore cond =new Semaphore(0);
	private int prox_count, cond_count;
	
	
	public void lock() throws InterruptedException{
		mutex.acquire();
	}
	
	public void unlock(){
		if (prox_count > 0){
			prossimo.release();
		}else{
			mutex.release();
		}
	}
	
	public void await() throws InterruptedException{
		cond_count++;
		if (prox_count > 0){
			prossimo.release();
		}else{
			mutex.release();
		}
		cond.acquire();
		cond_count--;
	}
	
	public void signal(){
		if ( cond_count>0 ) {
			prox_count++;
			cond.release();
			try {
				prossimo.acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			prox_count--;
			}
	}
	
	/*non credo che dovrebbe rilasciare tutti quei permessi , perchè in quel caso tutti i threads
	 * potrebbero entrare , bisogna diminuire
	 */
	public void signalAll(){
		if ( cond_count>0 ) {
			prox_count++;
			cond.release(cond_count);
			try {
				prossimo.acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			prox_count--;
			}
	}
}
