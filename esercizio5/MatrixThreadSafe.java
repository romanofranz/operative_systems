/**
 * 
 */
package esercizio5;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author romanofranz
 *
 */
public class MatrixThreadSafe {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		AtomicInteger[][]v=new AtomicInteger[10][10];
		RigaTS[] righe=new RigaTS[10];
		ColonnaTS[] col=new ColonnaTS[10];
		for(int i=0;i<righe.length;i++){
			righe[i]=new RigaTS(v[i]);
			righe[i].start();
		}
		for(int j=0;j<col.length;j++){
			col[j]=new ColonnaTS(j,v);
			col[j].start();
		}
		for(int k=0;k<righe.length;k++){
			System.out.println(Arrays.toString(v[k]));
		}
		

	}

}

class RigaTS extends Thread{

	AtomicInteger[] v;
	
	public RigaTS(AtomicInteger[]v){

		this.v=v;
	}
	public void run(){
		for(int i=0;i<v.length;i++)
			v[i].getAndDecrement();
	}
}

class ColonnaTS extends Thread{
	int a;
	AtomicInteger[][] v;
	
	public ColonnaTS(int a,AtomicInteger[][]v){
		this.a=a;
		this.v=v;
	}
	public void run(){
		for(int i=0;i<v.length;i++)
			v[i][a].getAndDecrement();
	}
}
