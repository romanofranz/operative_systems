/**
 * 
 */
package esercizio5;

import java.util.Arrays;

/**
 * @author romanofranz
 *
 */

class RigaNTS extends Thread {
	int[] r;
	public RigaNTS(int[]r) {
		this.r = r;
	}
	public void run() {
		for (int i = 0; i < r.length; i++)
			r[i]--;
	}
}

class ColonnaNTS extends Thread {
	int[][] A; int c;
	public ColonnaNTS(int[][] A, int c) {
		this.A = A; this.c = c;
	}
	public void run() {
		for (int i = 0; i < A.length; i++)
			A[i][c]++;
	}
}
public class MatrixNTS {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [][] Matrix=new int [10][9];
		int n=Matrix.length;
		int m=Matrix[0].length;
		RigaNTS[] righe = new RigaNTS[n];
		for(int i=0;i<n;i++){
			righe[i]=new RigaNTS(Matrix[i]);
			righe[i].start();
		}
		ColonnaNTS[] col= new ColonnaNTS[m];
		for(int j=0;j<m;j++){
			col[j]=new ColonnaNTS(Matrix,j);
			col[j].start();
		}
		for(int k=0;k<n;k++){
			System.out.println(Arrays.toString(Matrix[k]));
		}
		

	}

}
