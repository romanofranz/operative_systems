package esempi;

import esercizio9.HoareMonitor;

public class ContoCorrenteMyLock extends ContoCorrente{
	public ContoCorrenteMyLock(int dep){
		super(dep);
	}
	private HoareMonitor mon =new HoareMonitor();
	public void deposita(int importo)throws InterruptedException {
		mon.lock();
		try {
			deposito += importo;
			} finally {
				mon.unlock();
			}
	}
	public void preleva(int importo) throws InterruptedException {
		mon.lock();
	
		try {
			deposito -= importo;
		} finally {
			mon.unlock();
		}
	}
	public static void main(String...vabene){
		ContoCorrente cc = new ContoCorrenteMyLock(10000);
		try {
			cc.test(200,100,5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
