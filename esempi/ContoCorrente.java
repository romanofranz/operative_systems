package esempi;

public abstract class ContoCorrente {
	protected int deposito;
	protected final int depositoIniziale;
	public ContoCorrente(int depositoIniziale) {
		this.depositoIniziale = depositoIniziale;
		deposito = depositoIniziale;
	}
	public abstract void deposita(int importo) throws InterruptedException;
	public abstract void preleva(int importo)throws InterruptedException;
	protected void test(int numCorrentisti, int importo, int numOperazioni)
			throws InterruptedException {
			Correntista correntisti[] = new Correntista[numCorrentisti];
			for (int i = 0; i < numCorrentisti; i++) {
			correntisti[i] = new Correntista(this, importo, numOperazioni);
			}
			while (true) {
			Thread threadCorrentisti[] = new Thread[numCorrentisti];
			for (int i = 0; i < numCorrentisti; i++) {
			threadCorrentisti[i] = new Thread(correntisti[i]);
			threadCorrentisti[i].start();
			}
			for (int i = 0; i < numCorrentisti; i++) {
			threadCorrentisti[i].join();
			}
			if (deposito == depositoIniziale) {
			System.out.println("Corretto");
			} else {
			System.out.println("Errore");
			}
			}
	}
}
