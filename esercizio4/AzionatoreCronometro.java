package esercizio4;

import java.util.Scanner;

public class AzionatoreCronometro {

	public static void main(String[] args) {
		
		Scanner in= new Scanner(System.in);
		Cronometro c= new Cronometro();
		Thread t = new Thread(c);
		System.out.println("premi invio per cominciare");
		in.nextLine();
		t.start();
		do{
			System.out.println("premi stop per fermarti o un altra stringa per mettere in pausa");
			if(in.nextLine().equalsIgnoreCase("stop"))
				c.stop();
			else
				c.pausa();
		}while(!c.inStop());
		

	}

}
