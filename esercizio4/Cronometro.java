package esercizio4;

import java.util.concurrent.TimeUnit;

public class Cronometro implements Runnable {
	boolean pausa= false;
	boolean stop = false;
	int secondi =0;
	int pause=0;
	int secondiParziali=0;
	
	public void run(){
		for(;;){
			while(!inPausa() || !inStop()){
				try{
					TimeUnit.SECONDS.sleep(1);
					secondi++;
					secondiParziali++;
				}catch(InterruptedException e){
						break;
					}
				if(inPausa()){
					pause++;
					System.out.println("numero di secondi dall'ultima pausa "+secondiParziali);
					secondiParziali=0;
					pausa=false;
					break;
				}
				if(inStop()){
					System.out.println("Orologio fermo");
					System.out.println("numero di pause "+pause);
					return;
				}
			}
		}
	}
	
	public boolean inPausa(){
		return pausa;
	}
	
	public boolean inStop(){
		return stop;
	}
	
	public void pausa(){
		pausa=true;
	}
	public void riprendi(){
		pausa=false;
	}
	public void stop(){
		stop=true;
	}
	
	}
