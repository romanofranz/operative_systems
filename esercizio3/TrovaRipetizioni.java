package esercizio3;

public class TrovaRipetizioni extends Thread {
	
	private int x,y;
	private int nX=0;
	private int nY=0;
	private int []v;
	
	public TrovaRipetizioni(int[]v ,int x, int y){
		this.v=v;	this.x=x;	this.y=y;
	}
	
	public void run(){
		for(int i=0;i<v.length;i++){
			if(v[i]==x)
				nX++;
			if(v[i]==y)
				nY++;
		}
	}
	
	public int getX(){
		try {
			this.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return nX;
	}
	
	public int getY(){
		try {
			this.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return nY;
	}
}
