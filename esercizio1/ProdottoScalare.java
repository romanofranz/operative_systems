package esercizio1;

public class ProdottoScalare extends Thread {
	private int[]a,b;
	private int in,fin,p;
	
	public ProdottoScalare(int[]a,int[]b,int in,int fin){
		this.a=a;
		this.b=b;
		this.in=in;
		this.fin=fin;
	}
	
	public void run(){
		for(int i= in;i<fin;i++)
			p=p+a[i]*b[i];
	}
	
	public int getProdotto(){
		try {
			this.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return p;
	}

}
