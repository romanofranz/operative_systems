package esercizio1;

public class Applicazione_PS {

	public static void main(String[] args) {
		int[] a = {2, 3, 6, -5, 4, 1, 8, -3, 6};
		int[] b = {0, 1, 9, 2, -4, 8, -1, 2, 7};
		int n = a.length;
		int m = 3;
		int l = n / m;
		
		ProdottoScalare[] ps = new ProdottoScalare[m];
		for (int i = 0; i < m; i++) {
			ps[i] = new ProdottoScalare(a, b, i * l, (i + 1) * l);
			ps[i].start();
		}
		int res = 0;
		for (ProdottoScalare p: ps) res += p.getProdotto();
		System.out.println("Prodotto scalare: " + res);

	}

}
