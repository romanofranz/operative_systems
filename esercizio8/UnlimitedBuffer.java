package esercizio8;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class UnlimitedBuffer extends Buffer {
	private LinkedList<Elemento> buffer ;
	private Semaphore notEmpty;
	private Semaphore mutex;
	
	public UnlimitedBuffer(){
		super(0);
		notEmpty=new Semaphore(0);
		mutex=new Semaphore(1);
		buffer=new LinkedList<Elemento>();
	}
	
	public Elemento get() throws InterruptedException {
		notEmpty.acquire();
		mutex.acquire();
		Elemento e=buffer.removeLast();
		stampaStato();
		mutex.release();
		return e;
		}
	
	
	public void put(Elemento e) throws InterruptedException {
		mutex.acquire();
		buffer.addFirst(e);
		stampaStato();
		notEmpty.release();
		mutex.release();
	}
	
	private void stampaStato() {
		StringBuilder sb = new StringBuilder(buffer.size() * 2 + 10);
		sb.append("[");
		for (Elemento e: buffer)
			sb.append(e.getValore() + ", ");
		if (sb.length() > 1) sb.setLength(sb.length() - 2);
		sb.append("]");
		System.out.println(sb.toString());
	}

	
	public static void main(String[] args) {
		
		Buffer buff = new UnlimitedBuffer();
		int numProduttori = 10;
		int numConsumatori = 10;
		buff.test(numProduttori, numConsumatori);

	}

}
